// [SECTION] Dependencies and Modules
const User = require('../models/User')
const bcrypt = require('bcrypt');
const auth = require('../auth')

// [SECTION] Functionalities [Create]
	
	// Create a new user
	module.exports.registerUser = (reqBody) => {
		let fName = reqBody.firstName;
		let lName = reqBody.lastName;
		let mName = reqBody.middleName;
		let email = reqBody.email;
		let passW = reqBody.password;

		let newUser = new User ({
			firstName: fName,
			lastName: lName,
			middleName: mName,
			email: email,
			password: bcrypt.hashSync(passW, 10)
		});
		return newUser.save().then((user, error) => {
			if (user) {
				return `New user has been created with the following details
					${user}`;
			} else {
				return 'Failed to create a user.';
			}
		});
	};

	// Login user
		module.exports.loginUser = (reqBody) => {
		let uEmail = reqBody.email;
		let uPassW = reqBody.password;
		return User.findOne({email: uEmail}).then(result => {
			if (result === null) {
				// email does not exist
				return 'Email does not exist.'
			} else {
				let passW = result.password;
				const isMatched = bcrypt.compareSync(uPassW, passW);
				if (isMatched === true) {
					let dataNiUser = result.toObject();
					return {access: auth.createAccessToken(dataNiUser)};					
				} else {
					return 'Passwords does not match. Check credentials.'
				};
			};
		});
	};

// [SECTION] Functionalities [Retrieve]

	// Get all users
	module.exports.getAllUsers = () => {
		return User.find({}).then(result => {
			return result;
		})
	};

	// Get profile
	module.exports.getProfile = (id) => {
		return User.findById(id).then(user => {
			return user;
		});
	};


// [SECTION] Funtionalities [Update]

	// Set User as Admin
	module.exports.setAsAdmin = (userId) => {
		let updates = {
			isAdmin: true
		}
		return User.findByIdAndUpdate(userId, updates).then((admin, err) => {
			if (admin) {
				return true;
			} else {
				return 'Updates Failed to be implemented.';
			};
		});
	};

	// Set User as Non-Admin
	module.exports.setAsNonAdmin = (userId) => {
		let updates = {
			isAdmin: false
		}
		return User.findByIdAndUpdate(userId, updates).then((user, err) => {
			if (user) {
				return true;
			} else {
				return 'Failed to set admin as user.';
			};
		});
	};

	// Update Password
	module.exports.changePassword = (reqBody) => {
		let uEmail = reqBody.email;
		let uPassW = reqBody.password;
		return User.findOne({email: uEmail}).then(result => {
			if (result === null) {
				return "Email does not exist.";
			} else {
				let passW = result.password;
				const isMatched = bcrypt.compareSync(uPassW, passW);
				if (isMatched) {
					let newPass = reqBody.newpassword;
					return User.findOneAndUpdate({email: uEmail}, {password: bcrypt.hashSync(newPass, 10)}).then((savedUser, err) => {
						if (err) {
							return false;
						} else {
							return savedUser;
						};
					});
				} else {
					return "Password does not match. Check Credentials";
				};
			};
		});
	};