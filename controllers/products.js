//[SECTION] Dependencies and Modules
	const Product = require('../models/Product');

//[SECTION] Functionality [Create]
	// create product
	module.exports.addProduct = (info) => {
		let product = info.product;
		let pName = product.name;			
		let pDesc = product.description;
		let pCost = product.price;
		let newProduct = new Product ({
			name: pName,
			description: pDesc,
			price: pCost
		});
		return newProduct.save().then((savedProduct, err) => {
			if (savedProduct) {
				return savedProduct;
			} else {
				return false;
			}

		});
	};

//[SECTION] Functionality [Retrieve]
	// all products
	module.exports.getAllProducts = () => {
		return Product.find({}).then(result => {
			return result;
		});
	};

	// all active products
	module.exports.getAllActive = () => {
		return Product.find({isActive: true}).then(result => {
			return result;
		});

	};

	// get a single product
	module.exports.getProduct = (id) => {
		return Product.findById(id).then(result => {
			return result;
		});
	};

// [SECTION] Functionality [Update]
	
	// update product
	module.exports.updateProduct = (product, details) => {
		let pName = details.name;
		let pDesc = details.description;
		let pCost = details.price;		
		let updatedProduct = {
			name: pName,
			description: pDesc,
			price: pCost
		};
		let id = product.productId;
		return Product.findByIdAndUpdate(id, updatedProduct).then((productUpdated, err) => {
			if (productUpdated) {
				return true;
			} else {
				return 'Failed to update Product.';
			};
		});
	};
	
	// archive product
	module.exports.archiveProduct = (product) => {
			let id = product.productId;
			let updates = {
				isActive: false
			} 
		return Product.findByIdAndUpdate(id, updates).then((archived, err) => {
			if (archived) {
				return 'Product successfully archived.';

			} else {
				return false;
			}
		});
	};

	// reactivate product
	module.exports.reactivateProduct = (product) => {
			let id = product.productId;
			let updates = {
				isActive: true
			} 
		return Product.findByIdAndUpdate(id, updates).then((reactivated, err) => {
			if (reactivated) {
				return 'Product successfully re-activated.';

			} else {
				return false;
			}
		});
	};

// [SECTION] Functionality [Delete]
	// delete product
	module.exports.deleteProduct = (productId) => {
		return Product.findById(productId).then(product => {
			if (product === null ) {
				return 'No Resource was Found.';
			} else {
				return product.remove().then((removedProduct, err) => {
					if (err) {
						return 'Failed to remove product';
					} else {
						return 'Successfully Destroyed Data.';
					}
				});
			};
		});
	};