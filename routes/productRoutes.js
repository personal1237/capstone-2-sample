// [SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controllers/products');
	const auth = require('../auth');

// [SECTION] Routing Component
	const route = exp.Router();

// [SECTION] [POST] Route (Admin only)
	route.post('/create', auth.verify , (req, res) => {
		let isAdmin = auth.decode(req.headers.authorization).isAdmin
		let data = {
			product: req.body,
		}
		if (isAdmin) {
			controller.addProduct(data).then(outcome => {
				res.send(outcome)
			});
		} else {
			res.send('User Unauthorized to Proceed!');
		};
	});

// [SECTION] [GET] Route 
	// get all products
	route.get('/all', auth.verify , (req, res) => {
			let token = req.headers.authorization;
			let payload = auth.decode(token);
			let isAdmin = payload.isAdmin;
			isAdmin ? controller.getAllProducts().then(outcome => res.send(outcome))
			: res.send('User Unauthorized.')
		});

	// get all active products
	route.get('/', (req, res) => {
			controller.getAllActive().then(outcome => {
				res.send(outcome);
			});
		});

	// get a single product
	route.get('/:id', (req, res) => {
			let data = req.params.id;
			controller.getProduct(data).then(result => {
				res.send(result);
			});
		});

// [SECTION] [PUT] Route
	// update product
	route.put('/:productId', auth.verify, (req, res) => {
			let params = req.params;
			let body = req.body;
			if (!auth.decode(req.headers.authorization).isAdmin) {
				res.send('User Unauthorized');
			} else {
				controller.updateProduct(params, body).then(outcome => {
					res.send(outcome);
				});
			};
		});

	// archive product
	route.put('/:productId/archive', auth.verify , (req, res) => {
			let token = req.headers.authorization
			let isAdmin = auth.decode(token).isAdmin
			let params = req.params;
			(isAdmin) ? 
			controller.archiveProduct(params).then(result => {
				res.send(result);
			})
			:
			res.send('Unauthorized User');
		});

	// re-activate product
	route.put('/:productId/reactivate', auth.verify , (req, res) => {
			let token = req.headers.authorization
			let isAdmin = auth.decode(token).isAdmin
			let params = req.params;
			(isAdmin) ? 
			controller.reactivateProduct(params).then(result => {
				res.send(result);
			})
			:
			res.send('Unauthorized User');
		});

// [SECTION] [DELETE] Route
	route.delete('/:productId/delete', auth.verify , (req, res) => {
			let token = req.headers.authorization;
			let isAdmin = auth.decode(token).isAdmin;
			let id = req.params.productId;
			isAdmin ? controller.deleteProduct(id).then(result => res.send(result))
			: res.send('Unauthorized User.');
		});

// [SECTION] Export Route System
	module.exports = route;
