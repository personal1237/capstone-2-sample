// [SECTION] Dependencies and Modules
const mongoose = require('mongoose');

// [SECTION] Schema
const userBlueprint = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First Name is Required.']
	},
	lastName: {
		type: String,
		required: [true, 'Last Name is Required.']
	},
	middleName: {
		type: String,
		required: [true, 'Middle name is Required']
	},
	email: {
		type: String,
		required: [true, 'Email is Required.']
	},
	password: {
		type: String,
		required: [true, 'Password is Required.']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [
		{
			products: [
				{
					productName: {
						type: String,
						required: [true, 'Product Name Required']
					},
					quantity: {
						type: Number,
						required: [true, 'Quantity of products Required']
					}
				}

			],
			totalAmount: {
				type: Number,
				required: [true, 'Total amount required']
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]

});

// [SECTION] Model
module.exports = mongoose.model("User", userBlueprint);