const mongoose = require('mongoose');

const productBlueprint = new mongoose.Schema({
	name: {
		type: String,
		required: [true, 'Product Name required.']
	},
	description: {
		type: String,
		required: [true, 'Product Description required']
	},
	price: {
		type: Number,
		required: [true, 'Price of product is required.']
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders: [
		{
			orderId: {
				type: String,
				required: [true, 'Order ID required']
			}
		}		
	]
});

module.exports = mongoose.model("Product", productBlueprint);