// [SECTION] Dependencies and Modules
	const express = require('express');
	const mongoose = require('mongoose');
	const dotenv = require('dotenv');
	const userRoutes = require('./routes/userRoutes');
	const productRoutes = require('./routes/productRoutes');

// [SECTION] Environment Variables Setup
	dotenv.config();
	const port = process.env.PORT;
	const cred = process.env.MONGO_URL;

// [SECTION] Server Setup
	const app = express();
	app.use(express.json());
	app.use(cors()); //allow resource sharing to other projects
	app.use(express.urlencoded({extended: true}));

// [SECTION] Database Connect
	mongoose.connect(cred);
	const db = mongoose.connection;
	db.once('open', () => console.log('Connected to Mongo Atlas Database.'))

// [SECTION] Server Reoutes
	app.use('/users', userRoutes);
	app.use('/products', productRoutes);

// [SECTION] Server Responses
	app.get('/', (req, res) => {
		res.send (`Project deployed successfully.`)
	});
	app.listen(port, () => {
		console.log(`You are now live on port ${port}.`)
	});